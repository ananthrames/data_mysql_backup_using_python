#Steps:
#Create a backup directory with today's date in '%Y-%m-%d' format
#Create MYSQL dump in backup directory and compress with tar.gz extension
#Remove MYSQL dump file after compressing
#Compress data with tar.gz extension and copy to backup directory
#Upload backup directory to S3
#Remove backup directories which are older than 7 days


#Importing Libraries

import os
import subprocess
import time
import datetime
import pipes

#Setting global variables

backup_path = '/home/ec2-user/bp/backup'
backup_date = time.strftime('%Y-%m-%d')
backup_out_path = backup_path + '/' + backup_date

#Database backup

def backup_db():
    backup_db_host = 'localhost' 
    backup_db_user = 'root'
    backup_db_password = 'password'
    backup_db_name = 'testdb'
    mysqldump_command = "mysqldump -h " + backup_db_host + " -u " + backup_db_user + " -p" + backup_db_password + " " + backup_db_name + " > " + pipes.quote(backup_out_path) + "/" + backup_db_name + ".sql"
    os.system(mysqldump_command)
    dest = pipes.quote(backup_out_path) + '/' + pipes.quote(backup_db_name) + '.sql' + '.tar.gz'
    src = pipes.quote(backup_out_path) + '/' +  pipes.quote(backup_db_name) + '.sql'
    subprocess.call(['tar', '-czf', dest, src])
    
#Data backup & compression

def backup_tar():
    src="/home/ec2-user/bp/data"
    now = datetime.date.today()
    dest = pipes.quote(backup_out_path) + "/bkp-{}.tar.gz".format(now)
    subprocess.call(['tar', '-czf', dest, src])

#Create backup directory    

def backup_dircreate():
    subprocess.call(['mkdir', backup_out_path])

#Upload data and mysql backup to s3 where aws cli is already configured   

def backup_s3upload():
    s3bucket='s3://arr-testbucket'
    dest=s3bucket + '/' + backup_date
    subprocess.call(['aws', 's3', 'cp', backup_out_path, dest, '--recursive'])

#Remove backups older than 7 days from server

def backup_remove():
    #subprocess.call(['find', '/home/ec2-user/bp/master/*', '-mtime', '+7', '-exec', 'rm', '{}', '\;'])
    rm_command='find /home/ec2-user/bp/master/* -mtime +7 -exec rm {} \;'
    os.system(rm_command)
    
#Remove Myqsl backup after compressing

def backup_dumpremove():
    backup_db_name = 'testdb'
    src = pipes.quote(backup_out_path) + '/' +  pipes.quote(backup_db_name) + '.sql'
    subprocess.call(['rm', '-f', src])

backup_dircreate()
backup_db()
backup_dumpremove()
backup_tar()
backup_s3upload()
backup_remove()